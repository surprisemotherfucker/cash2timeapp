var App = angular.module("CashToTime",["ionic", "CashToTime.controllers", "CashToTime.services"]);



App.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home',{
            url: "/1",
            templateUrl: "home.html"
        })
        .state('tabs', {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html"
        })
        .state('tabs.easy', {
            url: "/easy",
            views: {
                'easy-tab': {
                    templateUrl: "templates/easy.html",
                }
            }
        })


        .state('tabs.love', {
            url: "/love",
            views: {
                'love-tab': {
                    templateUrl: "templates/love.html",
                }
            }
        })
        .state('tabs.party', {
            url: "/party",
            views: {
                'party-tab': {
                    templateUrl: "templates/party.html",
                }
            }
        })
        .state('tabs.extreme', {
            url: "/extreme",
            views: {
                'extreme-tab': {
                    templateUrl: "templates/extreme.html",
                }
            }
        })
        .state('carditem', {
            url: '/3',
            templateUrl: 'carditem.html',
            controller : "CardCtrl"
        })


    $urlRouterProvider.otherwise("/1");
    /*
     $httpProvider.defaults.useXDomain=true;
     $httpProvider.defaults.withCredentials=true;
     delete $httpProvider.defaults.headers.common['X-Requested-With'];
     $httpProvider.defaults.headers.common['Accept']='application/json';
     $httpProvider.defaults.headers.common['Content-Type']='application/json';*/
})

App.controller('CardCtrl', function($scope) {

})